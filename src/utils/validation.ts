/**
 * Checks if a given string represents a valid date in the 'YYYY-MM-DD' format
 * and is a valid date according to the calendar.
 * @param str
 */
export function isValidDateFormatAndDate(str: string) {
  if (!str) {
    return false
  }

  // Regular expression pattern for YYYY-MM-DD format
  const pattern = /^\d{4}-\d{2}-\d{2}$/

  // Check if the input matches the pattern
  if (!pattern.test(str)) {
    return false
  }

  // Try to create a Date object from the input
  const date = new Date(str)

  // Check if the Date object is valid and the year, month, and day components match
  // date.toISOString() returns a standardized date-time string in the format YYYY-MM-DDTHH:MM:SS.sssZ.
  return !isNaN(date.getTime()) && date.toISOString().slice(0, 10) === str
}
