import wrap from 'express-async-wrap'

import BaseController from '@/app/base/BaseController'
import GitHubService from '@/api/github/service/GitHubService'
import { isValidDateFormatAndDate } from '@/utils/validation'

export default class GitHubController extends BaseController {
  name = 'GitHub Controller'
  private readonly gitHubService: GitHubService

  constructor(options) {
    super(options)

    this.gitHubService = new GitHubService(options)
  }
  init() {
    /**
     * Search top repositories on GitHub
     */
    this.router.post('/search-top-repositories', wrap(this.searchTopRepositories.bind(this)))

    return this.router
  }

  /**
   * Search top Repositories on GitHub
   * @private
   */
  private async searchTopRepositories(req, res) {
    const { params } = req.body

    if (typeof params?.count !== 'number') {
      res.status(400).send('Invalid count. Should be number.')
      return
    }

    if (![10, 50, 100].includes(params?.count)) {
      res.status(400).send('Invalid count. Should be equal to 10 or 50 or 100.')
      return
    }

    if (!isValidDateFormatAndDate(params?.date)) {
      res.status(400).send('Invalid date. Should have a format YYYY-MM-DD.')
      return
    }

    const result = await this.gitHubService.searchTopRepositories(params)

    res.json(result)
  }
}
