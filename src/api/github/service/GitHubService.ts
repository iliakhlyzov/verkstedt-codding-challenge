import axios from 'axios'

import BaseService from '@/app/base/BaseService'
import { RepoSearchResultItem, SearchRepositoriesResponseSchema } from '@/api/github/0.types'

export default class GitHubService extends BaseService {
  name = 'GitHub Service'
  private readonly baseUrl = 'https://api.github.com'
  private readonly headers = {
    Accept: 'application/vnd.github+json',
    'X-GitHub-Api-Version': '2022-11-28',
  }

  /**
   * Search GitHub top repositories by params
   * @param params
   */
  public async searchTopRepositories(params): Promise<RepoSearchResultItem[]> {
    const { date, count } = params

    const key = `${date}`
    const cacheResult = await this.redis?.get(key)

    /**
     * Parsed json value has a structure {
     *     count: number,
     *     items: RepoSearchResultItem[]
     * }
     */
    if (cacheResult && count <= cacheResult.count) {
      return cacheResult.items.slice(0, count)
    }

    const queryParams = {
      q: `created:${date}`,
      sort: 'stars',
      order: 'desc',
      per_page: count,
      page: 1,
    }

    try {
      const response = await axios.get(`${this.baseUrl}/search/repositories`, {
        headers: this.headers,
        params: queryParams,
      })

      const { items: result } = response.data as SearchRepositoriesResponseSchema

      await this.redis?.set(key, JSON.stringify({ count, items: result }))

      return result
    } catch (err) {
      this.logger.error(`[${this.name}] Error in Axios Request: msg: ${err.message}, err: ${err.stack}`)
      throw err
    }
  }
}
