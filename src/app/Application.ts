import express from 'express'
import bodyParser from 'body-parser'
import compression from 'compression'
import winston from 'winston'
import 'dotenv/config'

import RouterRegister from '@/app/RouterRegister'
import RedisClient from '@/app/redis/RedisClient'

/**
 * Class to configure and start the application
 */
export class Application {
  private readonly config = this.setupConfig()
  private readonly logger = this.setupLogger()
  private redis

  public async start() {
    await this.setupRedis()

    const app = this.setupExpress()

    app.listen(this.config.port, () => {
      this.logger.info(`Server is running on port ${this.config.port}`)
    })
  }

  /**
   * Application termination function in case of unhandled errors.
   */
  public async shutdown(err, origin) {
    if (this.redis) {
      this.redis.quit(() => {
        this.logger.info('Disconnected from Redis')
      })
    }

    this.logger.error(`origin: ${origin}, ${err.message}, ${err.stack}`)
    process.exit(1)
  }

  private setupConfig() {
    const config = {
      env: process.env.NODE_ENV || 'development',
      port: process.env.PORT || '3333',
      redis: {
        ex: parseInt(process.env.REDIS_EXPIRE_TIME as string) || 60,
      },
    }

    return config
  }

  private setupLogger() {
    const logger = winston.createLogger({
      level: 'info',
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.printf((info) => {
          return `${info.timestamp} - ${info.level}: ${info.message}`
        }),
      ),
      transports: [
        //
        // - Write all logs with importance level of `error` or less to `error.log`
        // - Write all logs with importance level of `info` or less to `combined.log`
        //
        new winston.transports.File({ filename: 'logs/error.log', level: 'error' }),
        new winston.transports.File({ filename: 'logs/combined.log' }),
      ],
    })

    if (this.config.env === 'development') {
      logger.add(
        new winston.transports.Console({
          level: 'debug',
          format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.printf((info) => {
              return `${info.timestamp} - ${info.level}: ${info.message}`
            }),
          ),
        }),
      )
    }

    return logger
  }

  private async setupRedis() {
    const redisClient = new RedisClient({ logger: this.logger, config: this.config })
    await redisClient.connect()

    this.redis = redisClient
  }

  private setupExpress() {
    const app = express()

    app.disable('x-powered-by')

    app.use(bodyParser.json({ limit: '1mb' }))
    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(bodyParser.text({ limit: '1mb' }))
    app.use(compression())

    app.use((req, res, next) => {
      this.logger.info(`Incoming request: ${req.method} ${req.url}`)
      next()
    })

    // Middleware to log request duration
    if (this.config.env === 'development') {
      app.use((req, res, next) => {
        const startTime = Date.now()

        res.on('finish', () => {
          const duration = Date.now() - startTime
          this.logger.info(`${req.method} ${duration}ms - ${res.statusCode}`)
        })

        next()
      })
    }

    /**
     * Error logging middleware
     */
    const errorHandlerMiddleware = async (err, req, res, next) => {
      this.logger.error(`${err.message}; ${err.stack}`)

      this.config.env === 'development' ? res.status(500).send(`<pre>${err.stack}</pre>`) : res.status(500).end()
    }

    /**
     * Register routes
     */
    const routeRegister = new RouterRegister({ logger: this.logger, config: this.config, redis: this.redis })
    app.use('/', routeRegister.router)
    app.use(errorHandlerMiddleware)

    return app
  }
}
