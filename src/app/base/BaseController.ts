import { Router } from 'express'
import { AppNamespace } from '@/app/0.types'

/**
 * Base class for controllers
 */
export default class BaseController {
  name = 'Base Controller Class'
  protected readonly logger: AppNamespace.ILogger
  protected readonly config
  protected readonly _router = Router()

  constructor(options) {
    this.logger = options.logger
    this.config = options.config
  }

  /**
   * Function to register routes to the router
   * @private
   */
  protected init(): Router {
    throw new Error('Not implemented')
  }

  get router() {
    return this._router
  }
}
