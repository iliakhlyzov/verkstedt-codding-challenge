import { AppNamespace } from '@/app/0.types'
import { RedisClientType } from 'redis'

/**
 * Base class for services
 */
export default class BaseService {
  name = 'Base Service Class'
  protected readonly logger: AppNamespace.ILogger
  protected readonly config: any
  protected readonly redis: {
    set(key: string, value: any): void
    get(key: string): any
  }

  constructor(options) {
    this.logger = options.logger
    this.config = options.config
    this.redis = options.redis
  }
}
