import { createClient, RedisClientType } from 'redis'
import { AppNamespace } from '@/app/0.types'
import ILogger = AppNamespace.ILogger

export default class RedisClient {
  private readonly logger: ILogger
  private readonly config: any
  private readonly _client: RedisClientType
  private isConnected = false

  constructor(options) {
    this.logger = options.logger
    this.config = options.config

    this._client = createClient()

    this._client.on('error', (err) => {
      this.logger.error(`Redis Client Error, msg: ${err.message}, stack: ${err.stack}`)
      this.isConnected = false
    })

    this._client.on('connect', () => {
      this.logger.info('Connected to Redis server')
      this.isConnected = true
    })
  }

  public async connect() {
    if (this._client) {
      await this._client.connect()
    }
  }

  public async set(key: string, value: string) {
    if (!this.isConnected) {
      return
    }

    await this._client.set(key, value, { EX: this.config.redis.ex })
  }

  public async get(key: string) {
    if (!this.isConnected) {
      return null
    }

    const value = await this._client.get(key)
    const result = value ? JSON.parse(value) : null

    return result
  }
}
