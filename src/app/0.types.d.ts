export declare namespace AppNamespace {
  interface ILogger {
    info: (msg: string) => void
    debug: (msg: string) => void
    error: (msg: string) => void
    warn: (msg: string) => void
  }
}
