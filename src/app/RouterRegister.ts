import { Router } from 'express'

import GitHubController from '@/api/github/GitHubController'

/**
 * Class for routes registration
 */
export default class RouterRegister {
  private _router = Router()

  constructor(options) {
    this._router.use('/github', new GitHubController(options).init())
  }

  get router(): Router {
    return this._router
  }
}
