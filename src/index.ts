import { Application } from './app/Application'

async function run() {
  const app = new Application()

  try {
    await app.start()
  } catch (err) {
    await app.shutdown(err, null)
  }

  process.on('uncaughtException', async (err, origin) => {
    await app.shutdown(err, origin)
  })

  process.on('unhandledRejection', async (err) => {
    await app.shutdown(err, null)
  })
}

run().then(() => {})
