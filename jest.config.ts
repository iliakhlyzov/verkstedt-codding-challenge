import type { Config } from 'jest'

const config: Config = {
    preset: 'ts-jest',
    verbose: true,
    moduleNameMapper: {
        '^@/(.*)$': '<rootDir>/src/$1',
        '^@tests/(.*)$': '<rootDir>/tests/$1',
    },
};

export default config
