import { getApp } from '@tests/app/app'

let app

beforeAll(async () => {
  app = await getApp()
})

afterAll(async () => {
  await app.server.close()
})

afterEach(async () => {})

describe('POST /github/search-top-repositories POSITIVE', () => {
  const cases = [
    {
      setup: {
        count: 10,
        date: '2012-10-01',
      },
    },
    {
      setup: {
        count: 50,
        date: '2012-10-01',
      },
    },
    {
      setup: {
        count: 100,
        date: '2012-10-01',
      },
    },
  ]

  describe.each(cases)('Table testing', ({ setup }) => {
    it(`should return ${setup.count} repositories`, async () => {
      const res = await app.request.post('/github/search-top-repositories').send({
        params: {
          count: setup.count,
          date: setup.date,
        },
      })

      expect(res.statusCode).toBe(200)
      expect(res.body.length).toBe(setup.count)
      expect(res.body.every((x) => x.created_at.startsWith(setup.date)))
    })
  })
})

describe('POST /github/search-top-repositories NEGATIVE', () => {
  const cases = [
    {
      comment: 'count cannot be 0',
      setup: {
        count: 0,
      },
    },
    {
      comment: 'count should be only 10, 50, 100',
      setup: {
        count: 49,
      },
    },
    {
      comment: 'count cannot be negative',
      setup: {
        count: -1,
      },
    },
    {
      comment: 'count should have only type number',
      setup: {
        count: 'string',
      },
    },
    {
      comment: 'date should be date',
      setup: {
        count: 50,
        date: 10,
      },
    },
    {
      comment: 'date should have a valid format',
      setup: {
        count: 50,
        date: new Date(),
      },
    },
  ]

  describe.each(cases)('Table testing', ({ setup, comment }) => {
    it(`should get 400 because ${comment}`, async () => {
      const res = await app.request.post('/github/search-top-repositories').send({
        params: {
          count: setup.count,
          date: setup.date,
        },
      })

      expect(res.statusCode).toBe(400)
    })
  })
})
