import supertest from 'supertest'

import { Application } from '@/app/Application'

/**
 * This function provides run the server and returns testApp
 */
export async function getApp() {
  const application = new Application() as any
  const app = application.setupExpress()
  const request = supertest(app)

  const server = app.listen()

  return {
    server,
    request,
  }
}
