# Verkstedt Coding Challenge Backend

Welcome to the Verkstedt Coding Challenge Backend! This backend service provides functionality to discover top repositories on GitHub based on star count and other criteria. You can use this service to retrieve lists of repositories, apply filters, and utilize Redis caching for improved performance.

## Table of Contents

- [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
- [Usage](#usage)
    - [API Endpoint](#api-endpoint)
- [Redis Cache](#redis-cache)
- [TODO](#todo)
- [Contributing](#contributing)
- [License](#license)

## Getting Started

### Prerequisites

Before you begin, ensure you have the following installed on your machine:

- Node.js (v14 or higher)
- npm (Node Package Manager)
- Docker (if you plan to use Docker Compose)

### Installation

Clone this repository to your local machine:

   ```
   git clone https://github.com/your-username/verkstedt-coding-challenge-backend.git
   cd verkstedt-coding-challenge-backend
   npm install
   ```

# Usage

Ensure you have a running Redis server with default settings or run a Redis container using Docker Compose before starting the project.

If you haven't set up a Redis server, you can run a Redis container using Docker Compose:
```bash 
docker-compose up -d
```

To start the project, run the following command:
```
npm run dev
```


### API Endpoint
You can access the API at http://localhost:3333/github/search-top-repositories. 

Make a POST request to this endpoint with the following body:

```
{
  "params": {
    "count": 10, 
    "date": "2021-12-18"
  }
}
```
- count: Choose from 10, 50, or 100
- date: Specify the date in YYYY-MM-DD format.

# Redis Cache
This project integrates Redis caching for enhanced performance. Cached data is stored based on the date and count provided in the request. If the requested count is more than the cached count, the service will fetch data from the GitHub API and update the cache.

## TODO
- Adjust the build script (tsc builds a project with errors).
- Add automated ESLint correction and Prettier formatting.
- Write tests to validate Redis cache behavior.
- Consider using GitHub SDK or rework interaction with query options for the GitHub API.
- Set up CI/CD pipelines for automated testing and deployment.
- Create a Dockerfile for the Node.js project to run the environment using Docker. 

## Contributing
Contributions are welcome!

## License
This project is licensed under the ISC License.




